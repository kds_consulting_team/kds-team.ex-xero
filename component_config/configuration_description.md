**Parameters**

- endpoint --- list of endpoints to download
- modified_since --- [OPTIONAL] date (2020-01-01) or relative date (yesterday, -5 weeks, etc.) , only modified records will be fetched. 

Add or remove required endpoints:

```
{
  "modified_since": "-2 days",
  "endpoint": [
    		"Accounts",
                "BankTransfers",
                "BatchPayments",
                "BrandingThemes",
                "ContactGroups",
                "Currencies",
                "Employees",
                "ExpenseClaims",
                "InvoiceReminders",
                "Items",
                "LinkedTransactions",
                "Organisations",
                "Payments",
                "PaymentServices",
                "Quotes",
                "Receipts",
                "RepeatingInvoices",
                "Reports",
                "Setup",
                "TaxRates",
                "Users",
                "BankTransactions",
                "Contacts",
                "Invoices",
                "Overpayments",
                "Prepayments",
                "PurchaseOrders",
                "CreditNotes",
                "ManualJournals",
                "Journals"
  ]
}
```