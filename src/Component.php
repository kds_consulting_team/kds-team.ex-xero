<?php

namespace Keboola\Xero;

use Keboola\Component\BaseComponent;

class Component extends BaseComponent
{
    /**
     * @return string
     */
    protected function getConfigDefinitionClass(): string
    {
        return ConfigDefinition::class;
    }

    /**
     * @return Extractor
     * @throws Exception\UserException
     */
    public function initExtractor(): Extractor
    {
        $params = $this->getConfig()->getParameters();

        $authData = [
            'clientId' => $this->getConfig()->getOAuthApiAppKey(),
            'clientSecret' => $this->getConfig()->getOAuthApiAppSecret(),
            'oauthData' => $this->getConfig()->getOAuthApiData(),
        ];

        $state = $this->getInputState();

        if (isset($state['auth']['#refreshToken'])) {
            $authData['refreshToken'] = $state['auth']['#refreshToken'];
        }

        $ext = new Extractor($this->getLogger(), $authData);

        $this->writeOutputStateToFile(['auth' => ['#refreshToken' => $ext->getRefreshToken()]]);

        return $ext;
    }

    /**
     * @throws Exception\UserException
     */
    protected function run(): void
    {
        $params = $this->getConfig()->getParameters();

        $dirPath = $this->getDataDir() . '/out/tables';

        $this->initExtractor()->runExtraction($params, $dirPath);
    }
}
