<?php declare(strict_types=1);

namespace Keboola\Xero;

use GuzzleHttp;
use Keboola\Json\Analyzer;
use Keboola\Json\Exception\NoDataException;
use Keboola\Json\Parser;
use Keboola\Json\Structure;
use Keboola\Xero\Exception\UserException;
use Keboola\Xero\Exception\AuthException;
use League\OAuth2;
use League\OAuth2\Client\Token\AccessToken as AccessToken;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use XeroAPI\XeroPHP as Xero;

class Extractor
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Xero\Configuration
     */
    private $config;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $refreshToken;

    const URL_AUTHORIZE = 'https://api.xero.com/connections/';
    const URL_TOKEN = 'https://identity.xero.com/connect/token';

    /**
     * @var array
     */
    private $tenantIds;

    /**
     * Writer constructor.
     *
     * @param LoggerInterface $logger
     * @param array $authData
     * @throws Exception\UserException
     */
    public function __construct($logger, array $authData)
    {
        $this->logger = $logger;

        // A) Use unexpired access token directly
        // $this->config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken($authData['accessToken']);

        // B) Use refresh token to generate a new access token.
        //    Refresh token is valid for 60 days if not used. Used token is valid for a grace period of 30 minutes.
        $provider = new OAuth2\Client\Provider\GenericProvider([
            'clientId' => $authData['clientId'],
            'clientSecret' => $authData['clientSecret'],
            'redirectUri' => '',
            'urlAuthorize' => $this::URL_AUTHORIZE,
            'urlAccessToken' => $this::URL_TOKEN,
            'urlResourceOwnerDetails' => 'https://api.xero.com/connections/',
        ]);
        $oauthDataArray = json_decode($authData['oauthData']);
        if (isset($authData['refreshToken'])) {
            $this->refreshToken = $authData['refreshToken'];
        } else {
            $this->refreshToken = $oauthDataArray->refresh_token;
        }
        try {
            // refresh the token just in case
            $accessToken = $provider->getAccessToken('refresh_token', [
                'refresh_token' => $this->refreshToken,
            ]);
            $this->logger->info(sprintf("Token expires in: %s", $accessToken->getExpires()));
            $connections = $provider->getResourceOwner($accessToken)->toArray();
            $this->accessToken = $accessToken->getToken();
            $this->refreshToken = $accessToken->getRefreshToken();
        } catch (OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            throw new AuthException($e->getMessage(), 0, $e);
        }

        $this->config = Xero\Configuration::getDefaultConfiguration()->setAccessToken($this->accessToken);

        $this->logger->info(sprintf("Authorised for scopes: %s", $oauthDataArray->scope));

        $this->tenantIds = [];
        foreach ($connections as $connection) {
            $this->tenantIds[] = $connection['tenantId'];
        }
    }

    public function runExtraction($params, $dirPath): self
    {
        $parser = new Parser(new Analyzer(new NullLogger(), new Structure()));
        foreach ($this->tenantIds as $tenantId) {
            $this->logger->info(sprintf("Extracting tenantId: %s", $tenantId));
            $this->extract($params, $dirPath, $parser, $tenantId);
        }
        foreach ($parser->getCsvFiles() as $file) {
            copy($file->getPathName(), $dirPath . '/' . $file->getName());
        }
        return $this;
    }

    /**
     * @param array $params
     * @param string $dirPath
     * @return Extractor
     * @throws Exception\UserException
     */
    public function extract($params, $dirPath, $parser, $tenantId): self
    {
        if (!isset($params['endpoint'])) {
            return $this;
        }

        if (is_string($params['endpoint'])) {
            $endpoints = [$params['endpoint']];
        } else {
            if (is_array($params['endpoint'])) {
                $endpoints = $params['endpoint'];
            } else {
                throw new Exception\UserException('Invalid "endpoint" config');
            }
        }

        $api = new Xero\Api\AccountingApi(
            new GuzzleHttp\Client(),
            $this->config
        );

        // $single = ['Accounts', 'BankTransfers', 'BatchPayments', 'BrandingThemes', 'ContactGroups', 'Currencies', 'Employees', 'ExpenseClaims', 'InvoiceReminders', 'Items', 'LinkedTransactions', 'Organisation', 'Payments', 'PaymentServices', 'Quotes', 'Receipts', 'RepeatingInvoices', 'Reports', 'Setup', 'TaxRates', 'TrackingCategories', 'Users'];
        // $paged = ['BankTransactions','Contacts','Invoices','Overpayments','Prepayments','PurchaseOrders', 'CreditNotes', 'ManualJournals'];
        // $offsetted = ['Journals'];
        if (!isset($params['modified_since'])) {
            $modSince = '2000-01-01';
        } else {
            $modSince = $params['modified_since'];
        }
        $ifModifiedSince = strtotime($modSince) * 1000;
        $this->logger->info(sprintf("Last updated since: %s", $ifModifiedSince));

        foreach ($endpoints as $endpoint) {
            // TODO: support for $endpoint as a map with params

            switch ($endpoint) {
                // single endpoints
                case 'Accounts':
                case 'BankTransfers':
                case 'BatchPayments':
                case 'BrandingThemes':
                case 'ContactGroups':
                case 'Currencies':
                case 'Employees':
                case 'ExpenseClaims':
                case 'InvoiceReminders':
                case 'Items':
                case 'LinkedTransactions':
                case 'Organisations':
                case 'Payments':
                case 'PaymentServices':
                case 'Quotes':
                case 'Receipts':
                case 'RepeatingInvoices':
                case 'Reports':
                case 'Setup':
                case 'TaxRates':
                case 'Users':
                    $data = $this->fetch($api, $endpoint, $tenantId, function ($api, $tenantId) use (
                        $endpoint,
                        $ifModifiedSince
                    ) {
                        $method = 'get' . $endpoint;
                        return $api->$method($tenantId, $ifModifiedSince)->jsonSerialize();
                    });
                    break;
                case 'TrackingCategories':
                    $data = $this->fetch($api, $endpoint, $tenantId, function ($api, $tenantId) use (
                        $endpoint,
                        $ifModifiedSince
                    ) {
                        $method = 'get' . $endpoint;
                        return $api->$method($tenantId)->jsonSerialize();
                    });
                    break;
                // paged endpoints
                case 'BankTransactions':
                    $data = $this->fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, function (
                        $api,
                        $page,
                        $tenantId,
                        $ifModifiedSince
                    ) {
                        return $api->getBankTransactions($tenantId, $ifModifiedSince, null, null, $page)->jsonSerialize();
                    });
                    break;
                case 'Contacts':
                    $data = $this->fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, function (
                        $api,
                        $page,
                        $tenantId,
                        $ifModifiedSince
                    ) {
                        return $api->getContacts($tenantId, $ifModifiedSince, null, null, null, $page)->jsonSerialize();
                    });
                    break;
                case 'Invoices':
                    $data = $this->fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, function (
                        $api,
                        $page,
                        $tenantId,
                        $ifModifiedSince
                    ) {
                        return $api->getInvoices($tenantId, $ifModifiedSince, null, null, null, null, null, null, $page)->jsonSerialize();
                    });
                    break;
                case 'Overpayments':
                    $data = $this->fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, function (
                        $api,
                        $page,
                        $tenantId,
                        $ifModifiedSince
                    ) {
                        return $api->getOverpayments($tenantId, $ifModifiedSince, null, null, $page)->jsonSerialize();
                    });
                    break;
                case 'Prepayments':
                    $data = $this->fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, function (
                        $api,
                        $page,
                        $tenantId,
                        $ifModifiedSince
                    ) {
                        return $api->getPrepayments($tenantId, $ifModifiedSince, null, null, $page)->jsonSerialize();
                    });
                    break;
                case 'PurchaseOrders':
                    $data = $this->fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, function (
                        $api,
                        $page,
                        $tenantId,
                        $ifModifiedSince
                    ) {
                        return $api->getPurchaseOrders($tenantId, $ifModifiedSince, null, null, null, null, $page)->jsonSerialize();
                    });
                    break;
                case 'CreditNotes':
                    $data = $this->fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, function (
                        $api,
                        $page,
                        $tenantId,
                        $ifModifiedSince
                    ) {
                        return $api->getCreditNotes($tenantId, $ifModifiedSince, null, null, $page)->jsonSerialize();
                    });
                    break;
                case 'ManualJournals':
                    $data = $this->fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, function (
                        $api,
                        $page,
                        $tenantId,
                        $ifModifiedSince
                    ) {
                        return $api->getManualJournals($tenantId, $ifModifiedSince, null, null, $page)->jsonSerialize();
                    });
                    break;
                // offsetted endpoints
                case 'Journals':
                    $data = $this->fetchOffsetted($api, $endpoint, $tenantId,
                        $ifModifiedSince, function (
                            $api,
                            $offset,
                            $tenantId,
                            $ifModifiedSince
                        ) {
                            $journals = $api->getJournals($tenantId, $ifModifiedSince, $offset);
                            $offset = null;

                            if ($journals->count() > 0) {
                                $journalsArr = $journals->getJournals();
                                $offset = $journalsArr[count($journalsArr) - 1]->getJournalNumber();
                            }

                            return [$journals->jsonSerialize(), $offset];
                        });
                    break;
                default:
                    throw new Exception\UserException('Unknown endpoint: ' . $endpoint);
            }

            $this->logger->info(sprintf("Passed %d rows to parser", count($data)));

            if (count($data) === 0) {
                continue;
            }

            try {
                $parser->process($data, $endpoint);
            } catch (NoDataException $e) {
                throw new UserException('Process data: ' . $e->getMessage(), 0, $e);
            }
        }

        return $this;
    }

    /**
     * @param Xero\Api\AccountingApi $api
     * @param string $endpoint
     * @param callback $callback
     * @return array
     * @throws Exception\UserException
     */
    public function fetch($api, $endpoint, $tenantId, $callback): array
    {
        $this->logger->info(sprintf("Fetching %s...", $endpoint));

        $res = [];

        $count = 0;

        while ($count++ <= 1000) {
            try {
                $res = $callback($api, $tenantId);
            } catch (Xero\ApiException $e) {
                if ($e->getCode() === 429) {
                    $this->logger->notice("Rate limited, sleeping...");
                    sleep(5);
                    continue;
                } else {
                    throw new Exception\UserException('Fetch data from Xero API: ' . $e->getMessage(), 0, $e);
                }
            }

            break;
        }

        $this->logger->info(sprintf("Fetched %d rows of %s", count($res), $endpoint));

        return $res;
    }

    /**
     * @param Xero\Api\AccountingApi $api
     * @param string $endpoint
     * @param callback $callback
     * @return array
     * @throws Exception\UserException
     */
    public function fetchPaged($api, $endpoint, $tenantId, $ifModifiedSince, $callback): array
    {
        $this->logger->info(sprintf("Fetching %s...", $endpoint));

        $page = 1; // page 0 equals to page 1

        $res = [];

        while ($page <= 1000) {
            try {
                $data = $callback($api, $page, $tenantId, $ifModifiedSince);
            } catch (Xero\ApiException $e) {
                if ($e->getCode() === 429) {
                    $this->logger->notice("Rate limited, sleeping...");
                    sleep(5);
                    continue;
                } else {
                    throw new Exception\UserException('Fetch data from Xero API: ' . $e->getMessage(), 0, $e);
                }
            }

            $res = array_merge($res, $data);

            $this->logger->info(sprintf("Got %d data rows at page %d", count($data), $page));

            // To be tested: All API endpoints return up to 100 results, so we could break if count is not 100 and save one request.
            if (count($data) === 0) {
                break;
            }

            $page++;
        }

        $this->logger->info(sprintf("Fetched %d rows of %s", count($res), $endpoint));

        return $res;
    }

    /**
     * @param Xero\Api\AccountingApi $api
     * @param string $endpoint
     * @param callback $callback
     * @return array
     * @throws Exception\UserException
     */
    public function fetchOffsetted(
        $api,
        $endpoint,
        $tenantId,
        $ifModifiedSince,
        $callback
    ): array {
        $this->logger->info(sprintf("Fetching %s...", $endpoint));

        $count = 0;

        $offset = null;

        $res = [];

        while ($count <= 1000) {
            try {
                [$data, $offset] = $callback($api, $offset, $tenantId, $ifModifiedSince);
            } catch (Xero\ApiException $e) {
                if ($e->getCode() === 429) {
                    $this->logger->notice("Rate limited, sleeping...");
                    sleep(5);
                    continue;
                } else {
                    throw new Exception\UserException('Fetch data from Xero API: ' . $e->getMessage(), 0, $e);
                }
            }

            $res = array_merge($res, $data);

            $this->logger->info(sprintf("Got %d data rows at offset %d", count($data), $offset));

            // To be tested: All API endpoints return up to 100 results, so we could break if count is not 100 and save one request.
            if (count($data) === 0) {
                break;
            }

            $count++;
        }

        $this->logger->info(sprintf("Fetched %d rows of %s", count($res), $endpoint));

        return $res;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }
}
