<?php

namespace Keboola\Xero;

use Keboola\Component\Config\BaseConfigDefinition;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;

class ConfigDefinition extends BaseConfigDefinition
{

    /**
     * @return ArrayNodeDefinition
     */
    protected function getParametersDefinition() : ArrayNodeDefinition
    {
        $parametersNode = parent::getParametersDefinition();

        $parametersNode
            ->children()
                ->scalarNode('modified_since')->end()
                ->arrayNode('endpoint')
                    ->scalarPrototype()->end()
                ->end()
            ->end();

        return $parametersNode;
    }

}
