<?php

require __DIR__ . '/vendor/autoload.php';

use Keboola\Component\Logger;

$logger = new Logger();

try {
    $component = new Keboola\Xero\Component($logger);
    $component->execute();
//} catch(Keboola\Component\UserException $e) {
//    $logger->error($e->getMessage());
//
//    exit(1);
} catch(Throwable $e) {
    $logger->critical(
        get_class($e) . ':' . $e->getMessage(),
        [
            'errFile' => $e->getFile(),
            'errLine' => $e->getLine(),
            'errCode' => $e->getCode(),
            'errTrace' => $e->getTraceAsString(),
            'errPrevious' => $e->getPrevious() ? get_class($e->getPrevious()) : '',
        ]
    );
    // TODO: enable application error again
    exit(1);
    //exit(2);
}

exit(0);
