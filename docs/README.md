# XERO extractor - Keboola Connection

Connect your [XERO](https://www.xero.com/) account data with Keboola.

## User documentation

1. Create Keboola component configuration
2. Authorize using Keboola native authorization button
3. Set up configuration parameters - select entities you want to be extracted
```
{
  "endpoint": [
    "Accounts",
    "BankTransactions",
    "Contacts",
    "Invoices",
    "Overpayments",
    "Prepayments",
    "PurchaseOrders",
    "CreditNotes",
    "ManualJournals",
    "Journals"
  ],
}
```
