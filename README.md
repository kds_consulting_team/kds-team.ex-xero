# XERO extractor - Keboola Connection

The extractor will download data for all tenants that are authorized for the account used.

## Configuration


**Parameters**

- endpoint --- list of endpoints to download
- modified_since --- [OPTIONAL] date (2020-01-01) or relative date (yesterday, -5 weeks, etc.) , only modified records will be fetched. 

Add or remove required endpoints:

```
{
  "modified_since": "-2 days",
  "endpoint": [
    		"Accounts",
                "BankTransfers",
                "BatchPayments",
                "BrandingThemes",
                "ContactGroups",
                "Currencies",
                "Employees",
                "ExpenseClaims",
                "InvoiceReminders",
                "Items",
                "LinkedTransactions",
                "Organisations",
                "Payments",
                "PaymentServices",
                "Quotes",
                "Receipts",
                "RepeatingInvoices",
                "Reports",
                "Setup",
                "TaxRates",
                "Users",
                "BankTransactions",
                "Contacts",
                "Invoices",
                "Overpayments",
                "Prepayments",
                "PurchaseOrders",
                "CreditNotes",
                "ManualJournals",
                "Journals"
  ]
}
```

## Deployment

Gitlab  CI builds image from the Dockerfile and creates a new version of the component on every vX.Y.Z git tag.

## Keboola OAuth - XERO App

- Create App at [developer.xero.com](https://developer.xero.com/)
